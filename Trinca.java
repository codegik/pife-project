import java.util.ArrayList;

public class Trinca {
	private Carta cartas[];
	private int maxCartas = 3;
	private int tamamnho;
	
	public Trinca(Carta c1, Carta c2, Carta c3) {
		this.cartas 	= new Carta[maxCartas];
		this.cartas[0] 	= c1;
		this.cartas[1] 	= c2;
		this.cartas[2] 	= c3;
		this.tamamnho	= 3;
	}
	
	public Trinca(Carta c[]) {
		this.cartas = new Carta[maxCartas];
		for (int i = 0; i < this.cartas.length; i++) {
			this.cartas[i] = c[i];
			this.tamamnho++;
		}
	}
	
	public Trinca(ArrayList c) {
		this.cartas = new Carta[maxCartas];
		for (int i = 0; i < this.cartas.length; i++) {
			this.cartas[i] = (Carta)c.get(i);
			this.tamamnho++;
		}
	}
	
	public Trinca() {
		this.cartas 	= new Carta[this.maxCartas];
		this.tamamnho	= 0;
	}
	
	public boolean add(Carta c) {
		if (this.tamamnho < this.maxCartas) {
			for (int i = 0; i < this.tamamnho; i++) {
				if (this.cartas[i].getNaipe() == c.getNaipe() && 
						this.cartas[i].getNumero() == c.getNumero()) {
					return false;
				}
			}
			this.cartas[this.tamamnho] = c;
			this.tamamnho++;
			return true;
		} else {
			return false;
		}
	}
	
	public void remove(Carta c) {
		for (int i = 0; i < this.tamamnho; i++)
			if (this.cartas[i] == c)
				this.cartas[i] = null;
	}
	
	public void remove(int index) {
		this.cartas[index] = null;
	}
	

	public Carta getCarta(int index) {
		if (index < this.maxCartas) {
			return this.cartas[index];
		}
		return null;
	}
	
	public int tamanho() {
		return this.tamamnho;
	}
	
	public boolean isValido() {
		int n[] = new int[this.maxCartas];
		int v[] = new int[this.maxCartas];
		
		try {
			for (int i = 0; i < this.maxCartas; i++) {
				n[i] = this.cartas[i].getNaipe();
				v[i] = this.cartas[i].getNumero();
			}
			// verifica se os naipes sao iguais		
			if (n[0] == n[1] && n[0] == n[2]) {
				Ordena.ordenar(v);
				if ( (v[1] + 1) == v[2] && (v[1] - 1) == v[0] )
					return true;
			} else {
				if ( v[0] == v[1] && v[0] == v[2] )
					return true;
			}
		} catch(NullPointerException e) {
			e.getLocalizedMessage();
		}
		return false;
	}
	
	public boolean possuiCarta(Carta c) {
		for (int i = 0; i < this.tamamnho; i++)
			if (this.cartas[i] == c)
				return true;
		return false;
	}
	
	public void ordena() {
		Ordena.ordenar(this.cartas);
	}
}
