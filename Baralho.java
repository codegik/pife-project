import java.util.*;

public class Baralho implements Naipe {
	private int maxNipes;
	private static int maxCartas;
	private static ArrayList arrayCartas;
	private static Carta cartas[][];
	private static int nipes[] = {BASTO, COPA, ESPADA, OURO};
	
	public Baralho(int maxNaipe, int max) {
		this.maxNipes	= maxNaipe;
		maxCartas		= max;
		arrayCartas 	= new ArrayList();
		cartas 			= new Carta[this.maxNipes][maxCartas];
		for (int i = 0; i < this.maxNipes; i++) {
			for (int j = 0; j < maxCartas; j++) {
				// cria um objeto carta
				cartas[i][j] = new Carta(nipes[i], j + 1);
				// adiciona a carta ao array de cartas
				arrayCartas.add(cartas[i][j]);
			}
		}
		// duplica as cartas
		for (int i = 0; i < (maxCartas * this.maxNipes); i++) {
			arrayCartas.add(
				new Carta( ((Carta)arrayCartas.get(i)).getNaipe(), ((Carta)arrayCartas.get(i)).getNumero() )
			);
		}
	}
	
	/*
	 * embaralha as cartas
	 */
	public void embaralhar() {
		Random rnd = new Random();
		int max = arrayCartas.size();
		Carta temp[] = new Carta[max];
		for (int i = 0; i < max; i++) {
			// armazena as carta em um vetor temporário
			temp[i] = (Carta)arrayCartas.get(i);
		}
		arrayCartas.clear();
		ArrayList array = new ArrayList();
		boolean adicionar = true;
		int i = 0, index = 0;
		while (i < max) {
			index = rnd.nextInt(max);
			array.add("" + index);
			for (int j = 0; j < array.size()-1; j++) {
				if (index == Integer.parseInt((String)array.get(j))) {
					adicionar = false;
					break;
				}
			}
			if (adicionar) {
				arrayCartas.add(temp[index]);
				i++;
			} else {
				adicionar = true;
			}
		}		
	}

	/*
	 * remove uma carta do baralho passando como argumento um objeto
	 */
	public void removeCarta(Carta c) {
		arrayCartas.remove(c);
	}

	/*
	 * remove uma carta do baralho
	 */
	public void removeCarta(int index) {
		arrayCartas.remove(index);
	}

	/*
	 * remove uma carta do baralho
	 */
	public void removeUltimaCarta() {
		arrayCartas.remove(arrayCartas.size()-1);
	}

	public Carta getCarta(int index) {
		return (Carta)arrayCartas.get(index);
	}
	
	public Carta getUltimaCarta() {
		return (Carta)arrayCartas.get(arrayCartas.size()-1);
	}
	
	public static int getNumCartas() {
		return arrayCartas.size();
	}
	
	public static int getMaxCartas() {
		return maxCartas;
	}
	
	/*
	 * cria o mapeamento de uma carta
	 */
	public static ArrayList criarMapaDaCarta(Carta c) {
		ArrayList mapaDaCarta = new ArrayList();
		int index = 0, atual = c.getNumero()-1;
		if ( (c.getNumero()+1) <= 13 ) {
			mapaDaCarta.add(index, cartas[c.getNaipe()][(atual+1)]);
			index++;
		}
		if ( (c.getNumero()+2) <= 13 ) {
			mapaDaCarta.add(index, cartas[c.getNaipe()][(atual+2)]);
			index++;
		}
		if ( (c.getNumero()-1) >= 1 ) {
			mapaDaCarta.add(index, cartas[c.getNaipe()][(atual-1)]);
			index++;
		}
		if ( (c.getNumero()-2) >= 1 ) {
			mapaDaCarta.add(index, cartas[c.getNaipe()][(atual-2)]);
			index++;
		}
		for (int i = 0; i < nipes.length; i++) {
			if (c.getNaipe() != nipes[i]) {
				mapaDaCarta.add(index, cartas[nipes[i]][atual]);
				index++;
			}
		}
		return mapaDaCarta;
	}
}
