import java.util.*;

public class Lixo implements Naipe {
	private static ArrayList cartas;
	
	public Lixo() {
		cartas = new ArrayList();
	}
	
	public void addCarta(Carta c) {
		cartas.add(c);
	}

	public void removeCarta(Carta c) {
		cartas.remove(c);
	}

	public void removeCarta(int index) {
		cartas.remove(index);
	}

	public void removeUltimaCarta() {
		cartas.remove(cartas.size()-1);
	}

	public Carta getCarta(int index) {
		return (Carta)cartas.get(index);
	}
	
	public Carta getUltimaCarta() {
		return (Carta)cartas.get(cartas.size()-1);
	}

	public static int getNumCartas() {
		return cartas.size();
	}
	
	public static ArrayList getLixo() {
		return cartas;
	}
}