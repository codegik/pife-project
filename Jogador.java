import java.util.*;

import javax.swing.ImageIcon;

public class Jogador {
	protected ArrayList minhasCartas;
	protected ArrayList arrayTrincas;
	protected ArrayList numDeTrincas;
	protected ArrayList arrayTentativas;
	protected int maxCartas = 0;
	
	protected Jogador() {
		this.minhasCartas = new ArrayList();
		this.arrayTrincas = new ArrayList();
		this.numDeTrincas = new ArrayList();
		this.arrayTentativas = new ArrayList();
	}
	
	protected void addToCartas(Carta c) {
		this.minhasCartas.add(c);
	}
	
	protected void removeFromCartas(Carta c) {
		this.minhasCartas.remove(c);
	}
	
	/*
	 * retorna uma carta de acordo com sua figura
	 */
	protected Carta getCartaPelaFoto(String foto) {
		Carta c = null;
		for (int i = 0; i < this.minhasCartas.size(); i++) {
			c = (Carta)this.minhasCartas.get(i);
			if ( (Integer.toString(c.getNumero()) + Integer.toString(c.getNaipe())).equals(foto) ) {
				return c;
			}
		}
		return null;
	}
	
	/*
	 * retorna a imagem de acordo com a Carta
	 */
	protected ImageIcon getFotoPelaCarta(Carta c) {
		for (int i = 0; i < this.minhasCartas.size(); i++) {
			if ((Carta)this.minhasCartas.get(i) == c) {
				return ((Carta)this.minhasCartas.get(i)).getImage();
			}
		}
		return null;
	}
	
	/*
	 * verifica se a carta esta contida um alguma trinca
	 */
	protected boolean trincasPossuiCarta(Carta c) {
		if (this.arrayTrincas.size() > 0) {
			for (int i = 0; i < this.arrayTrincas.size(); i++) {
				if (((Trinca)this.arrayTrincas.get(i)).possuiCarta(c))
					return true;
			}
		}
		return false;
	}
	
	/*
	 * verifica se a carta de origem esta no mapeamento da carta destino
	 */
	protected boolean isMapeada(Carta orig, Carta dest) {
		ArrayList tmp = Baralho.criarMapaDaCarta(dest);
		for (int i = 0; i < tmp.size(); i++) {
			if (((Carta)tmp.get(i)).getNaipe() == orig.getNaipe() && 
				((Carta)tmp.get(i)).getNumero() == orig.getNumero()) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * cria as possíveis trincas das cartas
	 */
	protected void criarTrincas(int tentativas) {
		Carta orig = null;
		Carta dest = null;
		Trinca t = null;

		tentativas += 20;
		this.arrayTentativas.clear();
		this.numDeTrincas.clear();
		for (int n = 0; n < tentativas; n++) {
			this.embaralhar();
			this.maxCartas = this.minhasCartas.size();
			if (this.arrayTrincas.size() > 0) {
				this.arrayTrincas.clear();
			}
			for (int i = 0; i < this.maxCartas; i++) {
				t = null;
				t = new Trinca();
				orig = (Carta)this.minhasCartas.get(i);
				if (this.trincasPossuiCarta(orig)) {
					continue;
				}
				for (int j = 0; j < this.maxCartas; j++) {
					dest = (Carta)this.minhasCartas.get(j);
					if (this.trincasPossuiCarta(dest)) {
						continue;
					}
					if (this.isMapeada(orig, dest)) {
						t.add(orig);
						t.add(dest);
						if (t.tamanho() == 3) {
							if (t.isValido()) {
								this.arrayTrincas.add(t);
								t = null;
								t = new Trinca();
								j = this.maxCartas;
							} else {
								t.remove(dest);
							}
						}
					}
				}
			}
			// armazena uma sequencia de cartas
			this.arrayTentativas.add(new ArrayList(this.minhasCartas));
			// armazena a quantidade de trincas que a sequencia possui
			this.numDeTrincas.add("" + this.arrayTrincas.size());
		}
		
		// verifica qual a sequencia de cartas que possui o maior numero de trincas
        int aux = 0;
        for (int i = aux; i < (this.numDeTrincas.size()-1); i++) {
            if ( Integer.parseInt((String)this.numDeTrincas.get(aux)) > 
            	Integer.parseInt((String)this.numDeTrincas.get(i + 1)) ) {                 
                aux = i;
            } else {
            	aux = i + 1;
            }
        }
        
        this.minhasCartas.clear();
        this.minhasCartas.addAll((ArrayList)this.arrayTentativas.get(aux));
	}
	
	/*
	 * embaralha as cartas
	 */
	protected void embaralhar() {
		Random rnd = new Random();
		int max = this.minhasCartas.size();
		Carta temp[] = new Carta[max];
		for (int i = 0; i < max; i++) {
			// armazena as carta em um vetor temporário
			temp[i] = (Carta)this.minhasCartas.get(i);
		}
		this.minhasCartas.clear();
		ArrayList array = new ArrayList();
		boolean adicionar = true;
		int i = 0, index = 0;
		while (i < max) {
			index = rnd.nextInt(max);
			array.add("" + index);
			for (int j = 0; j < array.size()-1; j++) {
				if (index == Integer.parseInt((String)array.get(j))) {
					adicionar = false;
					break;
				}
			}
			if (adicionar) {
				this.minhasCartas.add(temp[index]);
				i++;
			} else {
				adicionar = true;
			}
		}		
	}
	
	/*
	 * retorna uma trinca
	 */
	protected Trinca getTrinca(int index) {
		return (Trinca)this.arrayTrincas.get(index);
	}
	
	/*
	 * retorna o numero de trincas
	 */
	protected int getNumTrincas() {
		return this.arrayTrincas.size();
	}
	
	/*
	 * retorna o numero de cartas na mao
	 */
	protected int getNumCartas() {
		return this.minhasCartas.size();
	}
	
	/*
	 * retorna uma carta
	 */
	protected Carta getCarta(int index) {
		return (Carta)this.minhasCartas.get(index);
	}

	/*
	 * ordena as cartas de acordo com as trincas
	 */
	protected void ordenaTrincas() {
		int cont = 0;
		for (int i = 0; i < this.getNumTrincas(); i++) {
			Trinca t = this.getTrinca(i);
			t.ordena();
			for (int j = 0; j < t.tamanho(); j++) {
				this.minhasCartas.remove(this.getPosicao(t.getCarta(j)));
				this.minhasCartas.add(cont, t.getCarta(j));
				cont++;				
			}
		}
		Carta orig = null, dest = null;
		boolean troca;
		int inicio = cont;
		for (int j = inicio; j < this.minhasCartas.size(); j++) {
			orig = (Carta)this.minhasCartas.get(j);
			this.minhasCartas.remove(this.getPosicao(orig));
			this.minhasCartas.add(cont, orig);
			cont++;
			troca = false;
			for (int i = j; i < this.minhasCartas.size(); i++) {
				dest = (Carta)this.minhasCartas.get(i);
				if (this.isMapeada(orig, dest)) {
					this.minhasCartas.remove(this.getPosicao(dest));
					this.minhasCartas.add(cont, dest);
					cont++;
					troca = true;
				}
			}
			if (troca) {
				j = cont-1;
			}
		}
	}
	
	/*
	 * retorna a posicao de uma carta do array de cartas da mao
	 */
	protected int getPosicao(Carta c) {
		for (int i = 0; i < this.getNumCartas(); i++) {
			if ((Carta)this.minhasCartas.get(i) == c)
				return i;
		}
		return -1;
	}
}
