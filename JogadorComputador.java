import java.util.ArrayList;

public class JogadorComputador extends Jogador implements Naipe {
	private Historico ht;
	private int maxCartas;
	private static ArrayList memoria;
	private static ArrayList maoHumano;
	
	public JogadorComputador() {
		super();
		this.ht = new Historico();
		this.maxCartas = 0;
		memoria = new ArrayList();
		maoHumano = new ArrayList();
	}
    
    /*
     * processa uma jogada e retorna a carta que retirou da mao
     */
	public Carta processar() {
		Carta c = null;
		this.criarTrincas(10);
		this.criarTabelaValores();
		this.maxCartas = this.minhasCartas.size();
		// retorna a pior carta da mao
		c = this.ht.getPiorCarta();
		if (c != null) {
			return c;
		} else {
			// retorna a primeira carta encontrada que nao faz parte de uma trinca
			for (int i = 0; i < this.maxCartas; i++) {
				c = (Carta)this.minhasCartas.get(i);
				if (!this.trincasPossuiCarta(c)) {
					return c;
				}
			}
		}
		return null;
	}
	
	/*
	 * cria a tabela de comparacoes das cartas
	 */
	public void criarTabelaValores() {
		Carta orig = null;
		Carta dest = null;
		this.maxCartas = this.minhasCartas.size();
		if (this.ht.tamanho() > 0)
			this.ht.clear();
		for (int i = 0; i < this.maxCartas; i++) {
			orig = (Carta)this.minhasCartas.get(i);
			// verifica se a carta origem esta presente em alguma trinca
			if (this.trincasPossuiCarta(orig)) {
				continue;
			}
			// adiciona uma carta a tabela de caracteristicas
			this.ht.add(orig);
			for (int j = 0; j < this.maxCartas; j++) {
				dest = (Carta)this.minhasCartas.get(j);
				// verifica se a carta esta no mapeamento
				if (this.isMapeada(orig, dest)) {
					// incrementa o numero de mapas da carta de origem
					this.ht.incNumMapas(orig);
				}
			}
		}
	}
	
	/*
	 * verifica se a carta eh boa
	 */
	public boolean processaCarta(Carta c) {
		// verifica se ja possui uma carta igual na mao
		for (int i = 0; i < this.minhasCartas.size(); i++) {
			if ( ((Carta)this.minhasCartas.get(i)).getNumero() == c.getNumero() &&
					((Carta)this.minhasCartas.get(i)).getNaipe() == c.getNaipe() ) {
				return false;
			}
		}
		// adiciona a carta a mao
		this.addToCartas(c);
		// cria as possiveis trincas
		this.criarTrincas(10);
		// cria a tabela de caracteristicas
		this.criarTabelaValores();
		// verifica se a carta inclusa esta em alguma trinca
		if (this.trincasPossuiCarta(c)) {
			this.removeFromCartas(c);
			return true;
		}
		// verifica se a carta c eh melhor do que as outras da mao
		else {
			Carta pior = this.ht.getPiorCarta();
			// verifica se ela nao eh a pior carta da mao
			if (pior != c) {
				if (this.ht.getNumMapas(c) == this.ht.getNumMapas(pior)) {
					this.removeFromCartas(c);
					return false;
				}
				this.removeFromCartas(c);
				return true;
			}
		}		
		this.removeFromCartas(c);
		return false;
	}
	
	public void addNaMemoria(Carta c) {
		memoria.add(c);
	}
	
	public void removeDaMemoria(Carta c) {
		memoria.remove(c);
	}

	public void removeDaMaoHumano(Carta c) {
		maoHumano.remove(c);
	}

	public void addNaMaoHumano(Carta c) {
		maoHumano.add(c);
	}

	/*
	 * retorna o numero de vezes que a carta se encontra na memoria da cpu
	 */
	public static int cartaNoLixo(Carta c) {
		int cont = 0;
		for (int i = 0; i < memoria.size(); i++) {
			if (c.getNaipe() == ((Carta)memoria.get(i)).getNaipe() && 
					c.getNumero() == ((Carta)memoria.get(i)).getNumero() ) {
				cont++;
			}
		}
		return cont;
	}
	
	/*
	 * retorna o numero de vezes que a carta se encontra na mao do jogador
	 */
	public static int cartaNaMaoHumano(Carta c) {
		int cont = 0;
		for (int i = 0; i < maoHumano.size(); i++) {
			if (c.getNaipe() == ((Carta)maoHumano.get(i)).getNaipe() && 
					c.getNumero() == ((Carta)maoHumano.get(i)).getNumero() ) {
				cont++;
			}
		}
		return cont;
	}
	
	
	public void printCartas() {
		for (int i = 0; i < this.minhasCartas.size(); i++) {
			System.out.print(((Carta)this.minhasCartas.get(i)).getNumero() + "" + ((Carta)this.minhasCartas.get(i)).getNaipe() + " ");
		}
		System.out.println();
	}
}
