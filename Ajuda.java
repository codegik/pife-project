import javax.swing.*;
import java.awt.Toolkit;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class Ajuda {
	private static final long serialVersionUID = -6895090175069700525L;
	private JFrame frame = null;
	private JPanel jContentPane = null;  //  @jve:decl-index=0:visual-constraint="37,10"
	private JLabel jLabel = null;
	private JLabel jLabel1 = null;
	private JLabel jLabel2 = null;
	private JLabel jLabel3 = null;
	private JLabel jLabel4 = null;
	private JLabel jLabel5 = null;
	private JLabel jLabel6 = null;
	private JLabel jLabel7 = null;
	private JLabel jLabel8 = null;
	private JLabel jLabel9 = null;
	private JPanel jPanel = null;
	private JLabel jLabel10 = null;
	private JLabel jLabel11 = null;
	private JLabel jLabel12 = null;
	/**
	 * This is the default constructor
	 */
	public Ajuda() {
		frame = new JFrame();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		frame.setSize(431, 480);
		frame.setResizable(false);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icone.png")));
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
		frame.setContentPane(getJContentPane());
		frame.setTitle("P IIII FE - Ajuda");
		frame.setVisible(false);
	}
	
	public void setVisible(boolean b) {
		frame.setVisible(b);
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel9 = new JLabel();
			jLabel9.setBounds(new java.awt.Rectangle(30,322,341,16));
			jLabel9.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
			jLabel9.setText("3) A carta 'A' (as) tem valor 1, ou seja, n�o pode ser 14.");
			jLabel8 = new JLabel();
			jLabel8.setBounds(new java.awt.Rectangle(30,303,338,16));
			jLabel8.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
			jLabel8.setText("2) Naipes iguais e valores diferentes e sequenciais.");
			jLabel7 = new JLabel();
			jLabel7.setBounds(new java.awt.Rectangle(30,284,328,16));
			jLabel7.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
			jLabel7.setText("1) Formado por 3 valores iguais e naipes diferentes.");
			jLabel6 = new JLabel();
			jLabel6.setBounds(new java.awt.Rectangle(30,258,324,23));
			jLabel6.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
			jLabel6.setForeground(new java.awt.Color(204,0,51));
			jLabel6.setText("Regras para formar Trincas:");
			jLabel5 = new JLabel();
			jLabel5.setBounds(new java.awt.Rectangle(30,127,373,118));
			jLabel5.setIcon(new ImageIcon(getClass().getResource("imagem.png")));
			jLabel5.setText("");
			jLabel4 = new JLabel();
			jLabel4.setBounds(new java.awt.Rectangle(30,105,192,19));
			jLabel4.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
			jLabel4.setForeground(new java.awt.Color(204,0,51));
			jLabel4.setText("Exemplos de Trincas:");
			jLabel3 = new JLabel();
			jLabel3.setBounds(new java.awt.Rectangle(30,82,364,16));
			jLabel3.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
			jLabel3.setText("Ganha quem primeiro conseguir alcan�ar este objetivo.");
			jLabel2 = new JLabel();
			jLabel2.setBounds(new java.awt.Rectangle(30,63,285,16));
			jLabel2.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
			jLabel2.setText("O objetivo � formar 3 trincas.");
			jLabel1 = new JLabel();
			jLabel1.setBounds(new java.awt.Rectangle(30,44,373,16));
			jLabel1.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
			jLabel1.setText("O pife se joga com 2 baralhos, e cada jogador recebe 9 cartas.");
			jLabel = new JLabel();
			jLabel.setBounds(new java.awt.Rectangle(15,9,216,26));
			jLabel.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 18));
			jLabel.setForeground(new java.awt.Color(0,102,102));
			jLabel.setText("Regras do PIIIIFE:");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.setBackground(java.awt.Color.white);
			jContentPane.setSize(new java.awt.Dimension(403,451));
			jContentPane.add(jLabel, null);
			jContentPane.add(jLabel1, null);
			jContentPane.add(jLabel2, null);
			jContentPane.add(jLabel3, null);
			jContentPane.add(jLabel4, null);
			jContentPane.add(jLabel5, null);
			jContentPane.add(jLabel6, null);
			jContentPane.add(jLabel7, null);
			jContentPane.add(jLabel8, null);
			jContentPane.add(jLabel9, null);
			jContentPane.add(getJPanel(), null);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jLabel12 = new JLabel();
			jLabel12.setBounds(new java.awt.Rectangle(22,52,324,16));
			jLabel12.setText("Computa��o - URI - campus de Santo �ngelo.");
			jLabel11 = new JLabel();
			jLabel11.setBounds(new java.awt.Rectangle(22,32,325,16));
			jLabel11.setText("In�cio Gomes Klassmann, aluno do curso de Ci�ncia da");
			jLabel10 = new JLabel();
			jLabel10.setBounds(new java.awt.Rectangle(10,7,174,16));
			jLabel10.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 18));
			jLabel10.setText("Desenvolvido por:");
			jPanel = new JPanel();
			jPanel.setBounds(new java.awt.Rectangle(23,360,377,82));
			jPanel.setLayout(null);
			jPanel.add(jLabel10, null);
			jPanel.add(jLabel11, null);
			jPanel.add(jLabel12, null);
		}
		return jPanel;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
