import javax.swing.ImageIcon;

public class Carta implements Naipe {
	private int naipe;
	private int numero;
	private ImageIcon image;
	
	public Carta(int naipe, int numero) {
		this.naipe 		= naipe;
		this.numero		= numero;
		this.image		= new ImageIcon(getClass().getResource("/cartas/" + numero + "" + naipe + ".png"));
	}
	
	public int getNaipe() {
		return this.naipe;
	}
	
	public int getNumero() {
		return this.numero;
	}	
	
	public ImageIcon getImage() {
		return this.image;
	}
	
	public String getImageName() {
		String nomeFoto = this.image.toString();
		nomeFoto = nomeFoto.substring(nomeFoto.lastIndexOf("/")+1, nomeFoto.lastIndexOf("."));
		return nomeFoto;
	}
}
