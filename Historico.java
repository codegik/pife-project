import java.util.ArrayList;

public class Historico implements Naipe {
	private ArrayList contMapas;	// armazena o numero de vezes que a carta faz parte de um mapeamento
	private ArrayList carta;		// armazena a carta
	
	public Historico() {
		this.carta = new ArrayList();
		this.contMapas = new ArrayList();
	}
	
	/*
	 * retorna uma carta de acordo com o valor informado
	 */
	public Carta get(int index) {
		return (Carta)this.carta.get(index);
	}
	
	/*
	 * retorna o numero de mapas de acordo com a carta informada
	 */
	public int getNumMapas(Carta c) {
		for (int i = 0; i < this.carta.size(); i++) {
			if (c == (Carta)this.carta.get(i)) {
				return Integer.parseInt((String)this.contMapas.get(i));
			}
		}
		return -1;
	}

	/*
	 * adiciona uma carta, o numero de mapas e o numero de trincas
	 */
	public void add(Carta c, int m) {
		this.carta.add(c);
		this.contMapas.add(""+m);
	}
	
	/*
	 * adiciona uma carta e por default o numero de mapas e trincas sera 0
	 */
	public void add(Carta c) {
		this.carta.add(c);
		this.contMapas.add(""+0);
	}
	
	/*
	 * muda o numero de mapas de acordo com a carta informada
	 */
	public void setNumMapas(Carta c, int valor) {
		for (int i = 0; i < this.carta.size(); i++) {
			if (c == (Carta)this.carta.get(i)) {
				this.contMapas.remove(i);
				this.contMapas.add(i, ""+valor);
			}
		}		
	}
	
	/*
	 * incrementa o numero de mapas da carta
	 */
	public void incNumMapas(Carta c) {
		for (int i = 0; i < this.carta.size(); i++) {
			if (c == (Carta)this.carta.get(i)) {
				int valor = Integer.parseInt((String)this.contMapas.get(i));
				valor++;
				this.contMapas.remove(i);
				this.contMapas.add(i, ""+valor);
			}
		}		
	}	
	
	/*
	 * remove uma carta do historico
	 */
	public void remove(Carta c) {
		for (int i = 0; i < this.carta.size(); i++) {
			if (((Carta)this.carta.get(i)) == c) {
				this.carta.remove(c);
				this.contMapas.remove(i);
			}
		}
	}
	
	/*
	 * retorna uma a pior carta
	 */
	public Carta getPiorCarta() {
		// verifica se existe cartas iguais na mao
		for (int i = 0; i < this.carta.size(); i++) {
			for (int j = 0; j < this.carta.size(); j++) {
				if ((Carta)this.carta.get(i) != (Carta)this.carta.get(j)) {
					if ( ((Carta)this.carta.get(i)).getNumero() == ((Carta)this.carta.get(j)).getNumero() && 
							((Carta)this.carta.get(i)).getNaipe() == ((Carta)this.carta.get(j)).getNaipe() ) {
						return (Carta)this.carta.get(j);
					}
				}
			}
		}
		
        Carta aux = null;
        int index = 0, nMapas1, nMapas2, nChances1, nChances2;
        aux = (Carta)this.carta.get(index);
        for (int i = index; i < (this.contMapas.size()-1); i++) {
        	nMapas1 = Integer.parseInt((String)this.contMapas.get(index));
        	nMapas2 = Integer.parseInt((String)this.contMapas.get(i + 1));
        	// verifica qual carta tem o menor numero de mapeamento
            if ( nMapas1 > nMapas2 ) {
            	aux = (Carta)this.carta.get(i + 1);
            	index = i + 1;
            }
            // se o numero de mapeamentos for igual, testa qual carta tem menor chance de jogo
            else if (nMapas1 == nMapas2) {
            	nChances1 = this.getSomaChances((Carta)this.carta.get(index));
            	nChances2 = this.getSomaChances((Carta)this.carta.get(i + 1));
            	// verifica qual carta tem menor chance de formar trinca
            	if ( nChances1 > nChances2 ) {
                	aux = (Carta)this.carta.get(i + 1);
                	index = i + 1;
            	} else if ( nChances1 < nChances2 ) {
                	aux = (Carta)this.carta.get(index);
            	}
            }
        }
        return aux;
	}
	
	
	/*
	 * soma as chances das cartas que fazem parte do mapeamento de c
	 */
	public int getSomaChances(Carta c) {
		int soma = 0;
		ArrayList temp = Baralho.criarMapaDaCarta(c);
		for (int i = 0; i < temp.size(); i++) {
			soma += this.chancesFormarTrinca((Carta)temp.get(i));
		}
		return soma;
	}
	
	/*
	 * retorna o numero de chances de jogo que a carta possui
	 */
	private int chancesFormarTrinca(Carta c) {
		ArrayList temp = this.getCartasFaltam(c);
		int chances = temp.size() * 2;
		for (int i = 0; i < temp.size(); i++) {
			switch ( JogadorComputador.cartaNoLixo((Carta)temp.get(i)) ) {
				case 1:
					chances -= 1;
				case 2:
					chances -= 2;
			}
			switch ( JogadorComputador.cartaNaMaoHumano((Carta)temp.get(i)) ) {
				case 1:
					chances -= 1;
					break;
				case 2:
					chances -= 2;
					break;
			}
		}
		return chances;
	}
	
	/*
	 * retorna um array com as cartas que faltam para formar uma trinca e que nao estao na mao
	 */
	private ArrayList getCartasFaltam(Carta c) {
		ArrayList mapa = Baralho.criarMapaDaCarta(c);
		ArrayList falta = new ArrayList();
		boolean adicionar;
		for (int i = 0; i < mapa.size(); i++) {
			adicionar = true;
			for (int j = 0; j < this.carta.size(); j++) {
				if ( (Carta)this.carta.get(j) != c ) {
					if ( ((Carta)mapa.get(i)).getNumero() == ((Carta)this.carta.get(j)).getNumero() && 
							((Carta)mapa.get(i)).getNaipe() == ((Carta)this.carta.get(j)).getNaipe() ) {
						adicionar = false;
						break;
					}
				}
			}
			if (adicionar) {
				// adiciona as cartas que nao estao na mao e que estao faltando para fazer trinca com c
				falta.add((Carta)mapa.get(i));
			}
		}
		return falta;
	}
	
	/*
	 * retorna o numero de carta no historico
	 */
	public int tamanho() {
		return this.carta.size();
	}
	
	/*
	 * limpa o historico
	 */
	public void clear() {
		this.carta.clear();
		this.contMapas.clear();
	}
}
