
public interface Naipe {
	public static final int BASTO 	= 0;
	public static final int COPA 	= 1;
	public static final int ESPADA	= 2;
	public static final int OURO	= 3;
	public static final int TRUE 	= 4;
	public static final int FALSE 	= 5;
}
