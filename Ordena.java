
public class Ordena {
	
    public static void ordenar(int[] vet) {
        boolean changed = true;
        int aux = 0;
        while (changed) {
            changed = false;
            for (int i = 0; i < (vet.length-1); i++) {
                if ( vet[i] > vet[i + 1] ) {                 
                    aux 		= vet[i];
                    vet[i] 		= vet[i+1];
                    vet[i+1]	= aux;        
                    changed 	= true;
                }
            }
        }
    }
    
    public static void ordenar(Carta[] vet) {
        boolean changed = true;
        Carta aux = null;
        while (changed) {
            changed = false;
            for (int i = 0; i < (vet.length-1); i++) {
                if ( vet[i].getNumero() > vet[i + 1].getNumero() ) {                 
                    aux 		= vet[i];
                    vet[i] 		= vet[i+1];
                    vet[i+1]	= aux;        
                    changed 	= true;
                }
            }
        }
    }
}
