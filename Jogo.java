import java.applet.AudioClip;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import java.awt.*;
import java.io.*;
import java.net.*;
import java.awt.Toolkit;


public class Jogo extends JFrame implements Naipe {
	private static final long serialVersionUID = -8562851706634707638L;
	private JPanel pnGlobal = null;
	private JPanel jPanel = null;
	private JButton novoJogo = null;
	private JButton ajuda = null;
	private JButton sair = null;
	private JPanel jPanel1 = null;
	private JLabel jLabel = null;
	private JPanel jPanel2 = null;
	private JPanel pnCartasCpu = null;
	private JPanel pnCartasHumano = null;
	private JPanel pnMesa = null;
	private JLabel imgBaralho = null;
	private JLabel imgLixo = null;
	private JLabel jLabel22 = null;
	private JLabel jLabel23 = null;
	private JLabel jLabel24 = null;
	private JLabel imgCartas[] = null;
	private JLabel imgCartasCpu[] = null;
	private JLabel status = null;
	private AudioClip som = null;
	private JScrollPane jScrollPane = null;
	private JLabel lbTrincas 		= null;
	private JTextArea movimentos 	= null;
	private Ajuda aj				= null;
	private JButton ordenar 		= null;
	
	
	/*
	 * *******************************************************************
	 * ***************** variaveis de manipulacao do jogo ****************
	 * *******************************************************************
	 */
	private Baralho baralho	 		= null;
	private Lixo lixo				= null;
	private Jogador humano  		= null;
	private JogadorComputador cpu 	= null;
	private int CPU					= 1;
	private int HUMANO				= 2;
	private int NOBODY				= 3;
	private String nomeFoto 		= null;
	private static int contMovimentosCpu = 0;
	private static boolean descartar;
	private static int jogador;
	private static boolean verCartasCpu; 
	
	
	
	
	
	
	
	
	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(1);
			gridLayout.setColumns(4);
			jPanel = new JPanel();
			jPanel.setBounds(new Rectangle(5,4,569,22));
			jPanel.setLayout(gridLayout);
			jPanel.add(getJButton(), null);
			jPanel.add(getJButton1(), null);
			jPanel.add(getJButton2(), null);
		}
		return jPanel;
	}

	/**
	 * This method initializes jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton() {
		if (novoJogo == null) {
			novoJogo = new JButton();
			novoJogo.setText("Novo jogo");
			novoJogo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// limpar os objetos de manipulacao do jogo
					contMovimentosCpu 	= 0;
					verCartasCpu		= false;
					baralho 			= null;
					lixo				= null;
					cpu					= null;
					humano				= null;
					descartar 			= false; 
					nomeFoto			= null;
					jogador				= NOBODY;
					baralho 			= new Baralho(4, 13);
					lixo				= new Lixo();
					cpu					= new JogadorComputador();
					humano				= new Jogador();
					// inicializar as imagens
					imgLixo.setIcon(null);
					imgBaralho.setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
					for (int i = 0; i < imgCartas.length; i++) {
						imgCartasCpu[i].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
						imgCartas[i].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
						imgCartas[i].setVisible(true);
					}
					imgCartas[imgCartas.length-1].setVisible(false);
					
					// embaralha as cartas
					baralho.embaralhar();
					// distribui as cartas
					for (int i = 0; i < 9; i++) {
						// distribui as cartas para o humano
						humano.addToCartas((Carta)baralho.getCarta(i));
						imgCartas[i].setIcon(((Carta)baralho.getCarta(i)).getImage());
						baralho.removeCarta(i);
					
						// distribui as cartas para a cpu
						cpu.addToCartas((Carta)baralho.getCarta(i));
						baralho.removeCarta(i);
					}
					jogador = HUMANO;
					status.setText("Voc� deve pegar uma carta do baralho");
					humano.criarTrincas(10);
					lbTrincas.setText("Trincas: " + humano.getNumTrincas());
					movimentos.setText("");
				}
			});
		}
		return novoJogo;
	}

	/**
	 * This method initializes jButton1	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton1() {
		if (ajuda == null) {
			ajuda = new JButton();
			ajuda.setText("Ajuda");
			ajuda.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					aj.setVisible(true);
				}
			});
		}
		return ajuda;
	}

	/**
	 * This method initializes jButton2	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton2() {
		if (sair == null) {
			sair = new JButton();
			sair.setText("Sair");
			sair.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
		}
		return sair;
	}

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jLabel = new JLabel();
			jLabel.setBounds(new Rectangle(688,2,39,29));
			jLabel.setIcon(new ImageIcon(getClass().getResource("icone.png")));
			jLabel.setText("");
			jLabel.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					if (verCartasCpu) {
						verCartasCpu = false;
						// esconde as cartas da cpu
						esconderCartas(cpu);
						
					} else {
						verCartasCpu = true;
						// mostra as cartas da cpu
						ordenarCartas(cpu);	
					}
				}
			});
			jPanel1 = new JPanel();
			jPanel1.setLayout(null);
			jPanel1.setBounds(new Rectangle(0,-1,741,33));
			jPanel1.setBackground(new Color(238,238,238));
			jPanel1.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			jPanel1.add(getJPanel(), null);
			jPanel1.add(jLabel, null);
		}
		return jPanel1;
	}

	/**
	 * This method initializes jPanel2	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel2() {
		if (jPanel2 == null) {
			GridLayout gridLayout4 = new GridLayout();
			gridLayout4.setRows(1);
			status = new JLabel();
			status.setText("Clique em Novo Jogo");
			status.setFont(new Font("Dialog", Font.BOLD, 16));
			status.setForeground(new Color(102,0,153));
			jPanel2 = new JPanel();
			jPanel2.setLayout(gridLayout4);
			jPanel2.setBounds(new Rectangle(4,435,736,30));
			jPanel2.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			jPanel2.add(status, null);
		}
		return jPanel2;
	}

	/**
	 * This method initializes jPanel3	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel3() {
		if (pnCartasCpu == null) {
			imgCartasCpu = new JLabel[10];
			for (int i = 0; i < 10; i++) {
				imgCartasCpu[i] = new JLabel();
				imgCartasCpu[i].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
				imgCartasCpu[i].setHorizontalAlignment(SwingConstants.CENTER);	
			}
			imgCartasCpu[9].setVisible(false);
			GridLayout gridLayout1 = new GridLayout();
			gridLayout1.setRows(1);
			gridLayout1.setColumns(10);
			pnCartasCpu = new JPanel();
			pnCartasCpu.setBackground(new Color(255,254,254));
			pnCartasCpu.setBorder(BorderFactory.createLineBorder(Color.gray,1));
			pnCartasCpu.setLayout(gridLayout1);
			pnCartasCpu.setBounds(new Rectangle(5,38,732,118));
			pnCartasCpu.add(imgCartasCpu[0], null);
			pnCartasCpu.add(imgCartasCpu[1], null);
			pnCartasCpu.add(imgCartasCpu[2], null);
			pnCartasCpu.add(imgCartasCpu[3], null);
			pnCartasCpu.add(imgCartasCpu[4], null);
			pnCartasCpu.add(imgCartasCpu[5], null);
			pnCartasCpu.add(imgCartasCpu[6], null);
			pnCartasCpu.add(imgCartasCpu[7], null);
			pnCartasCpu.add(imgCartasCpu[8], null);
			pnCartasCpu.add(imgCartasCpu[9], null);
		}
		return pnCartasCpu;
	}

	/**
	 * This method initializes jPanel4	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel4() {
		if (pnCartasHumano == null) {
			imgCartas = new JLabel[10];
			imgCartas[0] = new JLabel();
			imgCartas[0].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgCartas[0].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[0].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(0);
				}
			});
			imgCartas[1] = new JLabel();
			imgCartas[1].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[1].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgCartas[1].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(1);
				}
			});
			imgCartas[2] = new JLabel();
			imgCartas[2].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[2].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgCartas[2].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(2);
				}
			});
			imgCartas[3] = new JLabel();
			imgCartas[3].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[3].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgCartas[3].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(3);
				}
			});
			imgCartas[4] = new JLabel();
			imgCartas[4].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[4].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgCartas[4].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(4);
				}
			});
			imgCartas[5] = new JLabel();
			imgCartas[5].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[5].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgCartas[5].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(5);
				}
			});
			imgCartas[6] = new JLabel();
			imgCartas[6].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[6].setIcon(new ImageIcon(getClass().getResource("/cartas/baralho.png")));
			imgCartas[6].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(6);
				}
			});
			imgCartas[7] = new JLabel();
			imgCartas[7].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[7].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgCartas[7].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(7);
				}
			});
			imgCartas[8] = new JLabel();
			imgCartas[8].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[8].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgCartas[8].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(8);
				}
			});
			imgCartas[9] = new JLabel();
			imgCartas[9].setHorizontalAlignment(SwingConstants.CENTER);
			imgCartas[9].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgCartas[9].setVisible(false);
			imgCartas[9].addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					jogar(9);
				}
			});
			GridLayout gridLayout2 = new GridLayout();
			gridLayout2.setRows(1);
			gridLayout2.setColumns(10);
			pnCartasHumano = new JPanel();
			pnCartasHumano.setBackground(Color.white);
			pnCartasHumano.setBorder(BorderFactory.createLineBorder(Color.gray,1));
			pnCartasHumano.setLayout(gridLayout2);
			pnCartasHumano.setBounds(new Rectangle(5,304,733,120));
			pnCartasHumano.add(imgCartas[0], null);
			pnCartasHumano.add(imgCartas[1], null);
			pnCartasHumano.add(imgCartas[2], null);
			pnCartasHumano.add(imgCartas[3], null);
			pnCartasHumano.add(imgCartas[4], null);
			pnCartasHumano.add(imgCartas[5], null);
			pnCartasHumano.add(imgCartas[6], null);
			pnCartasHumano.add(imgCartas[7], null);
			pnCartasHumano.add(imgCartas[8], null);
			pnCartasHumano.add(imgCartas[9], null);
		}
		return pnCartasHumano;
	}

	/**
	 * This method initializes jPanel5	
	 * 	
	 * @return JPanel	
	 */
	private JPanel getJPanel5() {
		if (pnMesa == null) {
			jLabel22 = new JLabel();
			jLabel22.setVisible(true);
			imgLixo = new JLabel();
			imgLixo.setIcon(null);
			imgLixo.setHorizontalAlignment(SwingConstants.CENTER);
			imgLixo.addMouseListener(new MouseAdapter() {   
				public void mouseClicked(MouseEvent e) {
					if (e.getButton() == MouseEvent.BUTTON1) {
						if (jogador == HUMANO && imgLixo.getIcon() != null) {
							if (!descartar) {
								play("sons/cart3.wav");
								Carta c = (Carta)lixo.getUltimaCarta();
								humano.addToCartas(c);
								lixo.removeCarta(c);
								cpu.removeDaMemoria(c);
								cpu.addNaMaoHumano(c);
								if (Lixo.getNumCartas() > 0) {
									imgLixo.setIcon(((Carta)lixo.getUltimaCarta()).getImage());
								} else {
									imgLixo.setIcon(null);
								}
								for (int i = 0; i < 10; i++) {
									if (!imgCartas[i].isVisible()) {
										imgCartas[i].setVisible(true);
										imgCartas[i].setIcon(c.getImage());
									}
								}
								descartar = true;
							} else {
								play("sons/erro.wav");
							}
							status.setText("Voc� precisa descartar uma carta!");
							humano.criarTrincas(10);
							lbTrincas.setText("Trincas: " + humano.getNumTrincas());
						}
					}
				}
			});

			imgBaralho = new JLabel();
			imgBaralho.setHorizontalAlignment(SwingConstants.CENTER);
			imgBaralho.setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			imgBaralho.setText("");
			imgBaralho.addMouseListener(new MouseAdapter() {   
				public void mouseClicked(MouseEvent e) {
					if (e.getButton() == MouseEvent.BUTTON1) {
						if (jogador == HUMANO) {
							if (!descartar) {
								play("sons/cart3.wav");
								Carta c = (Carta)baralho.getUltimaCarta();
								humano.addToCartas(c);
								baralho.removeCarta(c);
								for (int i = 0; i < 10; i++) {
									if (!imgCartas[i].isVisible()) {
										imgCartas[i].setVisible(true);
										imgCartas[i].setIcon(c.getImage());
									}
								}
								status.setText("Voc� precisa descartar uma carta!");
								descartar = true;
							} else {
								play("sons/erro.wav");
							}
							humano.criarTrincas(10);
							lbTrincas.setText("Trincas: " + humano.getNumTrincas());
						}
					}
				}
			});
			GridLayout gridLayout3 = new GridLayout();
			gridLayout3.setRows(1);
			gridLayout3.setColumns(3);
			pnMesa = new JPanel();
			pnMesa.setBackground(Color.white);
			pnMesa.setBorder(BorderFactory.createLineBorder(Color.gray,1));
			pnMesa.setLayout(gridLayout3);
			pnMesa.setBounds(new java.awt.Rectangle(192,179,228,107));
			pnMesa.add(imgBaralho, null);
			pnMesa.add(jLabel22, null);
			pnMesa.add(imgLixo, null);
		}
		return pnMesa;
	}

	/**
	 * This is the default constructor
	 */
	public Jogo() {
		super();
		initialize();
		this.baralho 	= new Baralho(4, 13);
		this.lixo		= new Lixo();
		this.humano  	= new Jogador();
		this.cpu	 	= new JogadorComputador();
		jogador			= NOBODY;
		descartar		= false;
		humano.criarTrincas(10);
		lbTrincas.setText("Trincas: " + humano.getNumTrincas());
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("cavera.png")));
		this.setResizable(false);
		this.setSize(748, 500);
		this.setContentPane(getJContentPane());
		this.setTitle("P IIII FE");
		this.aj = new Ajuda();
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return JPanel
	 */
	private JPanel getJContentPane() {
		if (pnGlobal == null) {
			lbTrincas = new JLabel();
			lbTrincas.setBounds(new Rectangle(108,286,71,16));
			lbTrincas.setFont(new Font("Dialog", Font.PLAIN, 12));
			lbTrincas.setText("Trincas: 0");
			jLabel24 = new JLabel();
			jLabel24.setBounds(new Rectangle(6,285,102,16));
			jLabel24.setText("Jogador humano");
			jLabel23 = new JLabel();
			jLabel23.setBounds(new Rectangle(6,160,38,16));
			jLabel23.setText("CPU");
			pnGlobal = new JPanel();
			pnGlobal.setLayout(null);
			pnGlobal.add(getJPanel1(), null);
			pnGlobal.add(getJPanel2(), null);
			pnGlobal.add(getJPanel3(), null);
			pnGlobal.add(getJPanel4(), null);
			pnGlobal.add(getJPanel5(), null);
			pnGlobal.add(jLabel23, null);
			pnGlobal.add(jLabel24, null);
			pnGlobal.add(lbTrincas, null);
			pnGlobal.add(getJScrollPane(), null);
			pnGlobal.add(getOrdenar(), null);
		}
		return pnGlobal;
	}

	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return JScrollPane	
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setBounds(new java.awt.Rectangle(515,181,223,108));
			jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			jScrollPane.setFont(new Font("Dialog", Font.PLAIN, 12));
			jScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray,1), "Movimentos da CPU", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.PLAIN, 12), new Color(51,51,51)));
			jScrollPane.setViewportView(getMovimentos());
		}
		return jScrollPane;
	}

	/**
	 * This method initializes movimentos	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getMovimentos() {
		if (movimentos == null) {
			movimentos = new JTextArea();
			movimentos.setFont(new Font("Dialog", Font.PLAIN, 10));
			movimentos.setLineWrap(true);
			movimentos.setEditable(false);
			movimentos.setTabSize(4);
			movimentos.setWrapStyleWord(true);
		}
		return movimentos;
	}

	/**
	 * This method initializes ordenar	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getOrdenar() {
		if (ordenar == null) {
			ordenar = new JButton();
			ordenar.setBounds(new Rectangle(7,267,105,15));
			ordenar.setFont(new Font("Dialog", Font.PLAIN, 10));
			ordenar.setText("Ordenar cartas");
			ordenar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ordenarCartas(humano);
					lbTrincas.setText("Trincas: " + humano.getNumTrincas());
					
				}
			});
		}
		return ordenar;
	}

	
	/*
	 * *****************************************************************************
	 * ********************** IMPLEMENTACAO DO JOGO ********************************
	 * *****************************************************************************
	 */

	public void jogar(int index) {
		Carta c = null;
		// verifica se ja acabou as cartas do baralho
		if (Baralho.getNumCartas() == 0 && jogador != NOBODY) {
			jogador = NOBODY;
			imgBaralho.setIcon(null);
			// mostra as cartas ordenadas
			ordenarCartas(humano);
			ordenarCartas(cpu);
			play("sons/valeu.wav");
			status.setText("Fim de jogo!!  EMPATE!!");
		}
		
		if (jogador == HUMANO) {
			if (descartar) {
				play("sons/cart3.wav");
				nomeFoto = imgCartas[index].getIcon().toString();
				nomeFoto = nomeFoto.substring(nomeFoto.lastIndexOf("/")+1, nomeFoto.lastIndexOf("."));
				// retorna a carta que foi clicada
				c = humano.getCartaPelaFoto(nomeFoto);
				// adiciona a carta no lixo
				lixo.addCarta(c);
				// a cpu guarda na memoria a carta q foi para o lixo
				cpu.addNaMemoria(c);
				// a cpu remove das cartas q o humano comprou
				cpu.removeDaMaoHumano(c);
				// remove a carta da mao
				humano.removeFromCartas(c);
				// remove a imagem
				imgCartas[index].setVisible(false);
				// coloca a imagem no lixo
				imgLixo.setIcon(c.getImage());
				// informa que j� descartou a carta
				descartar = false;
				// muda de jogador
				jogador = CPU;
				status.setText("Voc� deve comprar uma carta do baralho ou pegar da mesa!");
				humano.criarTrincas(10);
				lbTrincas.setText("Trincas: " + humano.getNumTrincas());
				if (humano.getNumTrincas() == 3) {
					jogador  = NOBODY;
					status.setText("Fim de jogo!!  O vencedor foi o Humano!!");
					play("sons/valeu.wav");
					// mostra as cartas ordenadas
					ordenarCartas(humano);
					ordenarCartas(cpu);
				}
			} else {
				play("sons/erro.wav");
			}			
		}
		
		if (jogador == CPU) {
			contMovimentosCpu++;
			status.setText("A CPU est� processando a jogada!");
			// pega a carta do lixo
			c = lixo.getUltimaCarta();
			// testa se acarta do lixo for boa
			if (cpu.processaCarta(c)) {
				// retira uma carta do lixo
				lixo.removeCarta(c);
				// a cpu retira da memoria a carta que saiu do lixo
				cpu.removeDaMemoria(c);
				String s = c.getNumero() + " ";
				s +=c.getNaipe() == BASTO ? "basto" :
					c.getNaipe() == COPA ? "copas" : 
					c.getNaipe() == ESPADA ? "espada" :
					c.getNaipe() == OURO ? "ouro" : "";
				movimentos.append(contMovimentosCpu + ". pegou " + s + " do descarte.\n");
			} else {
				// pega uma carta do baralho
				c = baralho.getUltimaCarta();
				// retira uma carta do baralho
				baralho.removeCarta(c);	
				movimentos.append(contMovimentosCpu + ". comprou carta do baralho\n");
			}
			// adiciona a carta a mao
			cpu.addToCartas(c);
			// retorna a pior carta da mao
			c = cpu.processar();
			// coloca uma carta no lixo
			lixo.addCarta(c);
			// a cpu armazena na memoria a carta que foi para o lixo
			cpu.addNaMemoria(c);
			// atualiza a imagem do lixo
			imgLixo.setIcon(c.getImage());
			// remove da mao a carta descartada
			cpu.removeFromCartas(c);
			if (verCartasCpu) {
				ordenarCartas(cpu);
			}
			if (cpu.getNumTrincas() == 3) {
				jogador  = NOBODY;
				status.setText("Fim de jogo!!  O vencedor foi a CPU!!");
				play("sons/valeu.wav");
				// mostra as cartas da cpu
				ordenarCartas(cpu);		
			} else {
				status.setText("Voc� deve comprar uma carta ou pegar da mesa!");
				// muda de jogador
				jogador = HUMANO;
			}
		}
	}
	
	private void play(String s) {
		try {
			som = JApplet.newAudioClip(new URL("file:" + new File(s).toURL().getPath()));
			som.play();
		} catch (MalformedURLException e1) {
			System.out.println("Erro ao tocar som!");
		}
	}
	
	private void ordenarCartas(Jogador j) {
		j.criarTrincas(10);
		j.ordenaTrincas();
		for (int i = 0; i < j.getNumCartas(); i++) {
			if (j instanceof JogadorComputador) {
				imgCartasCpu[i].setVisible(true);
				imgCartasCpu[i].setIcon(j.getCarta(i).getImage());
			} else {
				imgCartas[i].setVisible(true);
				imgCartas[i].setIcon(j.getCarta(i).getImage());
			}
		}
		if (j.getNumCartas() == 9) {
			if (j instanceof JogadorComputador) {
				imgCartasCpu[9].setVisible(false);
			} else {
				imgCartas[9].setVisible(false);
			}
		}
	}
	
	private void esconderCartas(Jogador j) {
		j.ordenaTrincas();
		for (int i = 0; i < j.getNumCartas(); i++) {
			if (j instanceof JogadorComputador) {
				imgCartasCpu[i].setVisible(true);
				imgCartasCpu[i].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			} else {
				imgCartas[i].setVisible(true);
				imgCartas[i].setIcon(new ImageIcon(getClass().getResource("cartas/baralho.png")));
			}
		}
		if (j.getNumCartas() == 9) {
			if (j instanceof JogadorComputador) {
				imgCartasCpu[9].setVisible(false);
			} else {
				imgCartas[9].setVisible(false);
			}
		}
	}

	
	/*
	 * *****************************************************************************
	 * ********************** FIM DA IMPLEMENTACAO DO JOGO *************************
	 * *****************************************************************************
	 */

	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Jogo jogo = new Jogo();
		jogo.setVisible(true);
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"
